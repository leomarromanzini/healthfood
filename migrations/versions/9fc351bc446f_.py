"""empty message

Revision ID: 9fc351bc446f
Revises: 
Create Date: 2021-10-15 11:36:37.168993

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9fc351bc446f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('endereco',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('cidade', sa.String(length=50), nullable=False),
    sa.Column('estado', sa.String(length=50), nullable=False),
    sa.Column('rua', sa.String(length=50), nullable=False),
    sa.Column('numero', sa.String(length=50), nullable=False),
    sa.Column('complemento', sa.String(length=50), nullable=False),
    sa.Column('cep', sa.String(length=9), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('restaurante',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=211), nullable=False),
    sa.Column('estilo_culinario', sa.String(length=211), nullable=False),
    sa.Column('telefone', sa.String(length=14), nullable=False),
    sa.Column('descricao', sa.Text(), nullable=False),
    sa.Column('senha', sa.String(length=511), nullable=True),
    sa.Column('endereco_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['endereco_id'], ['endereco.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name'),
    sa.UniqueConstraint('telefone')
    )
    op.create_table('usuario',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('nome', sa.String(length=100), nullable=False),
    sa.Column('telefone', sa.String(length=14), nullable=False),
    sa.Column('email', sa.String(length=211), nullable=False),
    sa.Column('senha', sa.String(length=511), nullable=True),
    sa.Column('endereco_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['endereco_id'], ['endereco.id'], ),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email')
    )
    op.create_table('ordem',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('status', sa.Integer(), nullable=False),
    sa.Column('usuario_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['usuario_id'], ['usuario.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('pratos',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('nome', sa.String(length=100), nullable=False),
    sa.Column('ingredientes', sa.String(length=511), nullable=False),
    sa.Column('preço', sa.Float(), nullable=False),
    sa.Column('calorias', sa.Integer(), nullable=True),
    sa.Column('descricao', sa.Text(), nullable=False),
    sa.Column('id_restaurante', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['id_restaurante'], ['restaurante.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('order_dishes',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('order_id', sa.Integer(), nullable=True),
    sa.Column('dishes_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['dishes_id'], ['pratos.id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['order_id'], ['ordem.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('order_dishes')
    op.drop_table('pratos')
    op.drop_table('ordem')
    op.drop_table('usuario')
    op.drop_table('restaurante')
    op.drop_table('endereco')
    # ### end Alembic commands ###
