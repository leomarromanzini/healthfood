"""empty message

Revision ID: 087d763c1cad
Revises: e8f34ef5582e
Create Date: 2021-10-19 18:03:27.541819

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '087d763c1cad'
down_revision = 'e8f34ef5582e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('usuario', sa.Column('senha_hash', sa.String(length=511), nullable=True))
    op.drop_column('usuario', 'senha')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('usuario', sa.Column('senha', sa.VARCHAR(length=511), autoincrement=False, nullable=True))
    op.drop_column('usuario', 'senha_hash')
    # ### end Alembic commands ###
