from app.models.dishes_model import DishesModel
from app.services.verification_functions import confirm_json_format, confirm_existing_keys
from app.models.address_model import AddressModel
from flask import jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from sqlalchemy.exc import DataError
from app.models.restaurant_model import RestaurantModel
def get_all_dishes():
    pratos = DishesModel.query.all()
    return jsonify(pratos), 200

@jwt_required()
def register_new_dishe():
    data = request.get_json()
    json_correct_format = ['nome', 'ingredientes', 'preço', 'calorias', 'descricao']
    restaurant = get_jwt_identity()
    try:
        confirm_json_format(json_correct_format, **data)

        restaurant_found = RestaurantModel.query.filter(RestaurantModel.nome == restaurant.get('nome')).first()

        if not restaurant_found:
            raise IndexError


        data['id_restaurante'] = restaurant['id']
        new_dishe = DishesModel.create_new_dishe(**data)

        return jsonify(new_dishe), 201

    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except DataError:
        return {"Error": "Tipo de dado inválido."}, 400
    except IndexError:
        return {"Error": "Restaurante não encontrado."}, 404

@jwt_required()
def update_dishes_info(dishe_id: int):
    data = request.get_json()
    json_correct_format =  ['nome', 'ingredientes', 'preço', 'calorias', 'descricao']
    restaurant = get_jwt_identity()

    try:
        confirm_existing_keys(json_correct_format, **data)

        restaurant_found = RestaurantModel.query.filter(RestaurantModel.nome == restaurant.get('nome')).first()

        if not restaurant_found:
            raise IndexError


        updated_dishe = DishesModel.update_dishe_info(dishe_id, **data)

        return jsonify(updated_dishe), 200
    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except IndexError:
        return {"Error": "Prato não encontrado."}, 404

@jwt_required()    
def delete_dishe(dishe_id: int):
    restaurant = get_jwt_identity()

    restaurant_found = RestaurantModel.query.filter(RestaurantModel.nome == restaurant.get('nome')).first()

    if not restaurant_found:
        raise IndexError

    try:
        delete_message = DishesModel.delete(dishe_id)

        return {"msg": delete_message}, 200
    except IndexError:
        return {"Error": "Prato não encontrado."}, 404