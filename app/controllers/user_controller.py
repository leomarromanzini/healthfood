from flask import jsonify, request, current_app
from flask_jwt_extended.utils import get_jwt_identity
from flask_jwt_extended.view_decorators import jwt_required
from app.models.user_model import UserModel
from app.models.address_model import AddressModel
from app.services.verification_functions import confirm_json_format, confirm_existing_keys
from sqlalchemy.exc import IntegrityError, NoResultFound

def create_user():
    data = request.json
    valid_fields = ['nome', 'telefone', 'email', 'senha']

    try:
        confirm_json_format(valid_fields, **data)
    
        new_user = UserModel.create_new_user(**data)

        return jsonify(new_user), 201
    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 401  
    except TypeError:
        return {'Error': 'Invalid fields'}, 409
    except ValueError:
        return {"Error": "O telefone deve estar no formato (XX)xxxxx-xxxx"}, 400
    except IntegrityError:
        return {'Error': 'user email already exist'}, 409
def user_login():
    data = request.get_json()
    valid_fields = ['nome', 'senha']

    try:
        confirm_json_format(valid_fields, **data)

        access_token = UserModel.login(**data)

        return {"access_token": access_token}, 200

    except KeyError as e:
        return {"Error": "Campos de login inválidos, envie apenas o nome e senha do usere."}, 400
    except IndexError:
        return {"Error": "usuário não encontrado."}, 404
    except ValueError as e:
        message = e.args[0]
        return {"Error": message}, 401    
def get_all_users():

    users_list = UserModel.query.all()
    return jsonify(users_list), 200

def get_user_by_id(id: int):

    user = UserModel.query.get(id)

    if not user:
        return  {"Error": "Usuário não encontrado."}, 404

    user_info = {
            'nome': user.nome, 
            'telefone': user.telefone, 
            'email': user.email
    }

    if bool(user.endereco_id):
        actual_address = AddressModel.query.get(user.endereco_id)

        user_address = {
            'cidade': actual_address.cidade, 
            'estado': actual_address.estado, 
            'rua': actual_address.rua, 
            'numero': actual_address.numero, 
            'complemento': actual_address.complemento, 
            'cep': actual_address.cep   
        }

        user_info['endereco'] = user_address
    user_info['ordens'] = user.ordens
    return jsonify(user_info), 200

@jwt_required()
def update_user():
    data = request.json
    valid_fields = ['nome', 'telefone', 'email', 'senha']
    user = get_jwt_identity()

    try:
        confirm_existing_keys(valid_fields, **data)
        updated_user = UserModel.update_info(user.get('nome'), **data)

        
        return jsonify(updated_user), 200
    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except IndexError:
        return {"Error": "Usuário não encontrado."}, 404

@jwt_required()
def delete_user():
    user = get_jwt_identity()

    try:
        user = UserModel.delete(user.get('nome'))

        return jsonify(user), 200

    except NoResultFound:
        return {"Error": "Usuário não encontrado"}, 404

@jwt_required()
def create_user_address():
    data = request.get_json()
    valid_fields = ['cidade', 'estado', 'rua', 'numero', 'complemento', 'cep']
    user = get_jwt_identity()

    try:
        confirm_json_format(valid_fields, **data)
        

        user_found = UserModel.query.filter(UserModel.nome == user.get('nome')).first()

        if not user_found:
            raise IndexError

        new_address = AddressModel.create_new_address(**data)
        address_id = {'endereco_id': new_address.id}

        UserModel.update_info(user_found.nome, **address_id)

        address_response = {
            'cidade': new_address.cidade, 
            'estado': new_address.estado, 
            'rua': new_address.rua, 
            'numero': new_address.numero, 
            'complemento': new_address.complemento, 
            'cep': new_address.cep
        }

        return jsonify(address_response), 201
    except IndexError:
        return {"Error": "usuário não encontrado."}, 404
    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except ValueError:
        return {"Error": "CEP enviado está incorreto, deve estar no formato xxxxx-xxx."}, 400 

@jwt_required()
def update_user_address():
    data = request.get_json()
    valid_fields = ['cidade', 'estado', 'rua', 'numero', 'complemento', 'cep']
    user = get_jwt_identity()

    try:
        confirm_existing_keys(valid_fields, **data)

        user_addressed = UserModel.query.filter(UserModel.nome == user.get('nome')).first()

        if not user_addressed:
            raise IndexError
     
        update_user_address = AddressModel.update_address(user_addressed.endereco_id, **data)

        return jsonify(update_user_address), 200

    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except IndexError:
        return {"Error": "usuário não encontrado ou não possui um endereço vinculado."}, 404
    except ValueError:
        return {"Error": "CEP enviado está incorreto, deve estar no formato xxxxx-xxx."}, 400        
    
