from app.models.restaurant_model import RestaurantModel
from app.services.verification_functions import confirm_json_format, confirm_existing_keys
from app.models.address_model import AddressModel
from flask import jsonify, request
from sqlalchemy import exc
from flask_jwt_extended import jwt_required, get_jwt_identity

def register_restaurant():
    data = request.get_json()
    json_correct_format = ['nome', 'estilo_culinario', 'telefone', 'descricao', 'senha']


    try:
        confirm_json_format(json_correct_format, **data)

        new_restaurant = RestaurantModel.create_new_restaurant(**data)

        restaurant_response = {
            'nome': new_restaurant.nome, 
            'estilo_culinario': new_restaurant.estilo_culinario, 
            'telefone': new_restaurant.telefone, 
            'descricao': new_restaurant.descricao
        }

        return jsonify(restaurant_response), 201

    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except ValueError:
        return {"Error": "O telefone deve estar no formato (XX)xxxxx-xxxx"}, 400
    except exc.IntegrityError:
        return {"Error": "Já existe um restaurante com este nome ou telefone."}, 409

def restaurant_login():
    data = request.get_json()
    json_correct_format = ['nome', 'senha']

    try:
        confirm_json_format(json_correct_format, **data)

        access_token = RestaurantModel.login(**data)

        return {"access_token": access_token}, 200
    except KeyError as e:
        return {"Error": "Campos de login inválidos, envie apenas o nome e senha do restaurante."}, 400
    except IndexError:
        return {"Error": "Restaurante não encontrado."}, 404
    except ValueError as e:
        message = e.args[0]
        return {"Error": message}, 401

def get_all_restaurants_info():
    restaurantes = RestaurantModel.query.all()

    restaurants_info = [{
            'nome': restaurant.nome, 
            'estilo_culinario': restaurant.estilo_culinario, 
            'telefone': restaurant.telefone, 
            'descricao': restaurant.descricao,
            'pratos': restaurant.pratos
        } 
        for restaurant in restaurantes ]


    return jsonify(restaurants_info), 200

def get_restaurant_info(id: int):
    current_restaurant = RestaurantModel.query.get(id)

    if not current_restaurant:
        return {"Error": "Restaurante não encontrado."}, 404

    restaurant_info = {
            'nome': current_restaurant.nome, 
            'estilo_culinario': current_restaurant.estilo_culinario, 
            'telefone': current_restaurant.telefone, 
            'descricao': current_restaurant.descricao, 
    }

    if bool(current_restaurant.endereco_id):
        actual_address = AddressModel.query.get(current_restaurant.endereco_id)

        restaurant_address = {
            'cidade': actual_address.cidade, 
            'estado': actual_address.estado, 
            'rua': actual_address.rua, 
            'numero': actual_address.numero, 
            'complemento': actual_address.complemento, 
            'cep': actual_address.cep   
        }

        restaurant_info['endereco'] = restaurant_address

    restaurant_info['pratos'] = current_restaurant.pratos

    return jsonify(restaurant_info), 200

@jwt_required()
def create_restaurant_address():
    data = request.get_json()
    json_correct_format = ['cidade', 'estado', 'rua', 'numero', 'complemento', 'cep']
    restaurant = get_jwt_identity()

    try:
        confirm_json_format(json_correct_format, **data)

        restaurant_found = RestaurantModel.query.get(restaurant['id'])

        if not restaurant_found:
            raise IndexError

        new_address = AddressModel.create_new_address(**data)
        address_id = {'endereco_id': new_address.id}

        RestaurantModel.update_info(restaurant_found.nome, **address_id)

        address_response = {
            'cidade': new_address.cidade, 
            'estado': new_address.estado, 
            'rua': new_address.rua, 
            'numero': new_address.numero, 
            'complemento': new_address.complemento, 
            'cep': new_address.cep
        }

        return jsonify(address_response), 201
    except IndexError:
        return {"Error": "Restaurante não encontrado."}, 404
    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except ValueError:
        return {"Error": "CEP enviado está incorreto, deve estar no formato xxxxx-xxx."}, 400 

@jwt_required()
def update_restaurant_address():
    data = request.get_json()
    json_correct_format = ['cidade', 'estado', 'rua', 'numero', 'complemento', 'cep']
    restaurant = get_jwt_identity()

    try:
        confirm_existing_keys(json_correct_format, **data)

        restaurant_addressed = RestaurantModel.query.get(restaurant['id'])

        if not restaurant_addressed:
            raise IndexError

        update_restaurant_address = AddressModel.update_address(restaurant_addressed.endereco_id, **data)

        return jsonify(update_restaurant_address), 200

    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except IndexError:
        return {"Error": "Restaurante não encontrado ou não possui um endereço vinculado."}, 404
    except ValueError:
        return {"Error": "CEP enviado está incorreto, deve estar no formato xxxxx-xxx."}, 400        
    

@jwt_required()
def update_restaurant_info():
    data = request.get_json()
    json_correct_format = ['nome', 'estilo_culinario', 'telefone', 'descricao', 'senha']
    restaurant = get_jwt_identity()

    try:
        confirm_existing_keys(json_correct_format, **data)
        updated_restaurant = RestaurantModel.update_info(restaurant.get('nome'), **data)

        return jsonify(updated_restaurant), 200
    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except IndexError:
        return {"Error": "Restaurante não encontrado."}, 404

@jwt_required()    
def delete_restaurant():
    restaurant = get_jwt_identity()

    try:
        delete_message = RestaurantModel.delete(restaurant.get('nome'))

        return {"msg": delete_message}, 200
    except IndexError:
        return {"Error": "Restaurante não encontrado."}, 404