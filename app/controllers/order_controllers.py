from app.models.dishes_model import DishesModel
from app.models.order_model import OrderModel
from app.models.restaurant_model import RestaurantModel
from app.models.user_model import UserModel
from app.services.status_code import status_message
from app.services.verification_functions import (confirm_existing_keys,
                                                 confirm_json_format)
from flask import current_app, jsonify, request
from flask_jwt_extended import get_jwt_identity, jwt_required
from sqlalchemy import exc
from app.exeptions.exeptions import OrderNotFound

@jwt_required()
def get_all_orders():
    user = get_jwt_identity()
    orders = OrderModel.query.all()
    print(orders)
    orders_list = [{"id": order.id, "status": status_message(order.status), "pratos": order.pratos} for order in orders if order.usuario_id == user['id']]

    return jsonify(orders_list), 200

@jwt_required()
def create_new_order():
    data = request.get_json()
    user = get_jwt_identity()
    json_correct_format = ['status', 'pratos']

    try:
        confirm_json_format(json_correct_format, **data)

        data["usuario_id"] = user['id']
        dishes_id = data['pratos']
        data.pop('pratos')
        new_order = OrderModel.create(**data)

        for dish_id in dishes_id:
            if not DishesModel.query.get(dish_id):
                raise TypeError
            session = current_app.db.session
       
            prato = DishesModel.query.get(dish_id)
            order = OrderModel.query.get(new_order.id)

            order.pratos.append(prato)

            session.commit()

        return jsonify(new_order), 201

    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except exc.IntegrityError:
        return {"Error": "Já existe uma ordem igual a essa."}, 409
    except ValueError:
        return {"Error": "O código do estados não é valido, ele vai de 0 a 4"}, 400
    except TypeError:
        return {"Error": "Prato não encontrado"}, 404


@jwt_required()
def update_order_pratos(id: int):
    data = request.get_json()
    user = get_jwt_identity()
    json_correct_format = ['pratos']

    try:
        confirm_json_format(json_correct_format, **data)

        user_found = UserModel.query.get(user['id'])
        if not user_found:
            raise IndexError

        order = OrderModel.query.get(id)
        
        if not order:
            raise OrderNotFound

        dishes_id = data['pratos']

        for dish_id in dishes_id:
            if not DishesModel.query.get(dish_id):
                raise TypeError
            session = current_app.db.session

            prato = DishesModel.query.get(dish_id)

            order.pratos.append(prato)

            session.commit()
        
        return jsonify(order), 200
    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except IndexError:
        return {"Error": "Ordem ou usuário não encontrado."}, 404
    except TypeError:
        return {"Error": "Prato não encontrado"}, 404
    except OrderNotFound:
        return {"Error": "Ordem não encontrada"}, 404


@jwt_required()
def update_order_status(id: int):
    data = request.get_json()
    restaurant = get_jwt_identity()
    json_correct_format = ['status']

    try:
        confirm_existing_keys(json_correct_format, **data)

        restaurant_found = RestaurantModel.query.get(restaurant['id'])
        if not restaurant_found:
            raise IndexError

        updated_order = OrderModel.update_order_info(id, **data)
    
        return jsonify({"status": status_message(updated_order.status), "pratos": updated_order.pratos}), 200
    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except IndexError:
        return {"Error": "Ordem ou restaurante não encontrado."}, 404
    except ValueError:
        return {"Error": "O código do estados não é valido, ele vai de 0 a 4"}, 400

@jwt_required()    
def delete_order(order_id:int):
    try:
       
        delete = OrderModel.delete(order_id)

        return {"msg": delete}, 200
    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except IndexError:
        return {"Error": "Ordem não encontrada."}, 404

@jwt_required()
def delete_dishe_order(order_id: int):
    data = request.json

    try:
        dishes_id = data['pratos']
        

        for dish_id in dishes_id:

            order = OrderModel.query.get(order_id)

            if not DishesModel.query.get(dish_id):
                raise TypeError
            session = current_app.db.session
            
            prato = DishesModel.query.get(dish_id)
    
            order.pratos.remove(prato)

            session.commit()
        updated_order = OrderModel.query.get(order_id)
        
        return jsonify(updated_order), 2

    except KeyError as e:
        message = e.args[0]
        return {"Error": message}, 400
    except exc.IntegrityError:
        return {"Error": "Já existe uma ordem igual a essa."}, 409
    except AttributeError:
        return {"Error": "Ordem não encontrada"}, 400
    except TypeError:
        return {"Error": "Prato não encontrado"}, 404
