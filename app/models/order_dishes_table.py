from app.configs.database import db
from sqlalchemy import Column, Integer
from sqlalchemy.sql.schema import ForeignKey

order_dishes = db.Table('order_dishes',
    Column('id', Integer, primary_key=True),
    Column('order_id', Integer, ForeignKey('ordem.id', ondelete="CASCADE")),
    Column('dishes_id', Integer, ForeignKey('pratos.id', ondelete="CASCADE"))
)
