from app.configs.database import db
from flask import current_app
import re
from app.models.dishes_model import DishesModel
from dataclasses import dataclass
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import relationship, backref, validates
from flask_jwt_extended import create_access_token

@dataclass
class RestaurantModel(db.Model):
    id: int
    nome: str
    estilo_culinario: str
    telefone: str
    descricao: str
    endereco_id: str
    pratos: DishesModel

    __tablename__ = 'restaurante'

    id = Column(Integer, primary_key=True)
    
    nome = Column(String(211), nullable=False, unique=True)
    estilo_culinario = Column(String(211), nullable=False)
    telefone = Column(String(14), nullable=False, unique=True)
    descricao = Column(Text, nullable=False)
    senha_hash = Column(String(511), nullable=True)

    endereco_id = Column(Integer, ForeignKey("endereco.id"))
    pratos = relationship('DishesModel', backref=backref("restaurante", uselist=True))

    @validates('telefone')
    def validate_phone(self, key, phone):
        regex = "\(\d{2}\)\d{5}\-\d{4}"
        valid_phone = re.fullmatch(regex, phone)

        if not valid_phone:
            raise ValueError
        
        return phone
    
    @property
    def senha(self):
        raise AttributeError("Senha inacessível!")

    @senha.setter
    def senha(self, password_to_hash):
        self.senha_hash = generate_password_hash(password_to_hash)

    def verify_password(self, password_to_compare):
        return check_password_hash(self.senha_hash, password_to_compare)

    @classmethod
    def create_new_restaurant(cls, **data):
        password_to_hash = data.pop('senha')

        new_restaurant = cls(**data)
        new_restaurant.senha = password_to_hash

        session = current_app.db.session
        session.add(new_restaurant)
        session.commit()

        return new_restaurant
    
    @classmethod
    def login(cls, **data):
        found_restaurant = cls.query.filter(RestaurantModel.nome == data['nome']).first()

        if not found_restaurant:
            raise IndexError

        if found_restaurant.verify_password(data['senha']):
            acess_token = create_access_token(identity=found_restaurant)
            return acess_token
        else:
            raise ValueError('Senha Inválida')

    @classmethod
    def update_info(cls, restaurant_nome: str, **data):
        update_restaurant = cls.query.filter_by(nome=restaurant_nome).first()

        if not update_restaurant:
            raise IndexError

        password_to_hash = data.pop('senha', None)

        for key, value in data.items():
            setattr(update_restaurant, key, value)

        if bool(password_to_hash):
            update_restaurant.senha = password_to_hash

        session = current_app.db.session
        session.add(update_restaurant)
        session.commit()

        return update_restaurant

    @classmethod
    def delete(cls, restaurant_nome: str):
        deleted_restaurant = cls.query.filter_by(nome=restaurant_nome).first()

        if not deleted_restaurant:
            raise IndexError

        restaurant_nome = deleted_restaurant.nome
        
        message = "O restaurante {nome} foi deletado.".format(nome=restaurant_nome)

        session = current_app.db.session
        session.delete(deleted_restaurant)
        session.commit()

        return message