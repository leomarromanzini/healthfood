from app.configs.database import db
from flask import current_app
from re import fullmatch
from dataclasses import dataclass
from sqlalchemy import Column, Integer, String
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import relationship, backref, validates
from werkzeug.security import generate_password_hash, check_password_hash
from flask_jwt_extended import create_access_token

from app.models.order_model import OrderModel

@dataclass
class UserModel(db.Model):

    id: int
    nome: str
    telefone: str
    email: str
    endereco_id: str
    ordens: OrderModel

    __tablename__ = 'usuario'

    id = Column(Integer, primary_key=True)
    nome = Column(String(100), nullable=False)
    telefone = Column(String(14), nullable=False)
    email = Column(String(211), nullable=False, unique=True)
    senha_hash = Column(String(511), nullable=True)

    endereco_id = Column(Integer, ForeignKey("endereco.id"))
    ordens = relationship('OrderModel', backref=backref("usuarios", uselist=True))

    @validates('telefone')
    def validate_phone(self, key, phone):
        regex = "\(\d{2}\)\d{5}\-\d{4}"
        valid_phone = fullmatch(regex, phone)

        if not valid_phone:
            raise ValueError
        
        return phone
    
    @property
    def senha(self):
        raise AttributeError("Senha inacessível!")

    @senha.setter
    def senha(self, password_to_hash):
        self.senha_hash = generate_password_hash(password_to_hash)

    def verify_password(self, password_to_compare):
        return check_password_hash(self.senha_hash, password_to_compare)

    @classmethod
    def create_new_user(cls, **data):
        password_to_hash = data.pop('senha')

        new_user = cls(**data)
        new_user.senha = password_to_hash

        session = current_app.db.session
        session.add(new_user)
        session.commit()

        return new_user
    
    @classmethod
    def login(cls, **data):
        found_user = cls.query.filter(UserModel.nome == data['nome']).first()

        if not found_user:
            raise IndexError

        if found_user.verify_password(data['senha']):
            acess_token = create_access_token(identity=found_user)
            return acess_token
        else:
            raise ValueError('Senha Inválida')

    @classmethod
    def update_info(cls, user_nome: str, **data):
        update_user = cls.query.filter_by(nome=user_nome).first()

        if not update_user:
            raise IndexError

        password_to_hash = data.pop('senha', None)

        for key, value in data.items():
            setattr(update_user, key, value)

        if bool(password_to_hash):
            update_user.senha = password_to_hash

        session = current_app.db.session
        session.add(update_user)
        session.commit()

        return update_user

    @classmethod
    def delete(cls, user_nome: str):
        deleted_user = cls.query.filter_by(nome=user_nome).first()

        if not deleted_user:
            raise IndexError

        user_nome = deleted_user.nome
        
        message = "O usuário {nome} foi deletado.".format(nome=user_nome)

        session = current_app.db.session
        session.delete(deleted_user)
        session.commit()

        return message