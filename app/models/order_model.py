from dataclasses import dataclass

from app.configs.database import db
from app.models.dishes_model import DishesModel
from flask import current_app
from sqlalchemy import Column, Integer
from sqlalchemy.orm import relationship, validates
from sqlalchemy.sql.schema import ForeignKey


@dataclass
class OrderModel(db.Model):
    id: int
    status: int
    pratos: DishesModel
    usuario_id: int
    __tablename__ = 'ordem'

    id = Column(Integer, primary_key=True)
    
    status = Column(Integer, nullable=False)

    usuario_id = Column(Integer, ForeignKey("usuario.id"), nullable=False)
    pratos = relationship("DishesModel", secondary='order_dishes', back_populates='ordens')


    @validates('status')
    def validate_phone(self, key, status):
        if status < 0 or status > 5:
            raise ValueError
        return status

    @classmethod
    def create(cls, **data):
        new_order = cls(**data)

        session = current_app.db.session
        session.add(new_order)
        session.commit()

        return new_order


    @classmethod
    def update_order_info(cls, order_id: str, **data):
        update_order = cls.query.get(order_id)

        if not update_order:
            raise IndexError

        for key, value in data.items():
            setattr(update_order, key, value)

        session = current_app.db.session
        session.add(update_order)
        session.commit()

        return update_order

    @classmethod
    def delete(cls, order_usuario: int):
        deleted_order = cls.query.get(order_usuario)

        if not deleted_order:
            raise IndexError
        
        message = "A ordem foi deletada."

        session = current_app.db.session
        session.delete(deleted_order)
        session.commit()

        return message
