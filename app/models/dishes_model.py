from app.configs.database import db
from dataclasses import dataclass
from sqlalchemy import Column, Integer, String, Float, Text
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import relationship
from flask import current_app

@dataclass
class DishesModel(db.Model):

    id: int
    nome: str
    ingredientes: str
    preço: float
    calorias: int
    descricao: str
    id_restaurante: int

    __tablename__ = "pratos"

    id = Column(Integer, primary_key=True)
    nome = Column(String(100), nullable=False)
    ingredientes = Column(String(511), nullable=False)
    preço = Column(Float, nullable=False)
    calorias = Column(Integer, nullable=True)
    descricao = Column(Text, nullable=False)
    id_restaurante = Column(Integer, ForeignKey("restaurante.id"), nullable=False)
    ordens = relationship("OrderModel", secondary='order_dishes', back_populates='pratos')


    @classmethod
    def create_new_dishe(cls, **data):
        new_dishe = cls(**data)

        session = current_app.db.session
        session.add(new_dishe)
        session.commit()

        return new_dishe

    @classmethod
    def update_dishe_info(cls, dishe_id: int, **data):
        update_dishe = cls.query.get(dishe_id)

        if not update_dishe:
            raise IndexError


        for key, value in data.items():
            setattr(update_dishe, key, value)


        session = current_app.db.session
        session.add(update_dishe)
        session.commit()

        return update_dishe

    @classmethod
    def delete(cls, dishe_id: int):
        deleted_dishe = cls.query.get(dishe_id)

        if not deleted_dishe:
            raise IndexError

        dishe_nome = deleted_dishe.nome
        
        message = "O prato {nome} foi deletado.".format(nome=dishe_nome)

        session = current_app.db.session
        session.delete(deleted_dishe)
        session.commit()

        return message