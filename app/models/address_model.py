from app.configs.database import db
from dataclasses import dataclass
from sqlalchemy import Column, Integer, String
from flask import current_app
from sqlalchemy.orm import validates
import re

@dataclass
class AddressModel(db.Model):
    id: int
    cidade: str
    estado: str
    rua: str
    numero: str
    complemento: str
    cep: str

    __tablename__ = 'endereco'

    id = Column(Integer, primary_key=True)

    cidade = Column(String(50), nullable=False)
    estado = Column(String(50), nullable=False)
    rua = Column(String(50), nullable=False)
    numero = Column(String(50), nullable=False)
    complemento = Column(String(50), nullable=True)
    cep = Column(String(9), nullable=False)

    @validates('cep')
    def validate_phone(self, key, cep):
        regex = "\d{5}\-\d{3}"
        valid_cep = re.fullmatch(regex, cep)

        if not valid_cep:
            raise ValueError
        
        return cep

    @classmethod
    def create_new_address(cls, **data):
        new_address = cls(**data)

        session = current_app.db.session
        session.add(new_address)
        session.commit()

        return new_address

    @classmethod
    def update_address(cls, endereco_id: int, **data):
        updated_address = cls.query.get(endereco_id)

        for key, value in data.items():
            setattr(updated_address, key, value)

        session = current_app.db.session
        session.add(updated_address)
        session.commit()

        return updated_address