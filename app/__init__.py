from flask import Flask
from app.configs import env_configs, database as db, migration, jtw_auth
from app.routes import dishes_blueprint, user_blueprint, restaurant_blueprint, order_blueprint

def create_app():

    app = Flask(__name__)
    
    jtw_auth.init_app(app)
    env_configs.init_app(app)
    db.init_app(app)
    migration.init_app(app)

    app.register_blueprint(restaurant_blueprint.bp_restaurant)
    app.register_blueprint(user_blueprint.bp)
    app.register_blueprint(dishes_blueprint.bp_dishes)
    app.register_blueprint(order_blueprint.bp_order)

    return app