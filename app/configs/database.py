from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

def init_app(app: Flask):
   
    db.init_app(app)
    app.db = db

    from app.models.user_model import UserModel
    from app.models.restaurant_model import RestaurantModel
    from app.models.address_model import AddressModel
    from app.models.dishes_model import DishesModel
    from app.models.order_model import OrderModel
    from app.models.order_dishes_table import order_dishes