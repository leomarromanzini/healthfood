from app.models.restaurant_model import RestaurantModel
from flask_httpauth import HTTPTokenAuth
from flask import current_app

auth = HTTPTokenAuth(scheme='Bearer')


@auth.verify_token
def verify_token(api_key):
    user = RestaurantModel.query.filter_by(api_key=api_key).first()

    if user == None:
        return False
    return True