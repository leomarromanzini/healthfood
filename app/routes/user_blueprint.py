from flask import Blueprint
from app.controllers.user_controller import create_user, create_user_address, delete_user, get_user_by_id, get_all_users, update_user, update_user_address, user_login

bp = Blueprint('categories_bp', __name__, url_prefix='/user')

bp.post('/signup')(create_user)
bp.post('/signin')(user_login)
bp.get('')(get_all_users)
bp.get('/<int:id>')(get_user_by_id)
bp.patch('')(update_user)
bp.delete('')(delete_user)
bp.post('/address')(create_user_address)
bp.patch('/address')(update_user_address)