from flask import Blueprint
from app.controllers.dishes_controller import get_all_dishes, register_new_dishe,update_dishes_info,delete_dishe

bp_dishes = Blueprint("dishes", __name__, url_prefix="/dishes")


bp_dishes.get("")(get_all_dishes)
bp_dishes.post("")(register_new_dishe)
bp_dishes.patch("/<int:dishe_id>")(update_dishes_info)
bp_dishes.delete("/<int:dishe_id>")(delete_dishe)
