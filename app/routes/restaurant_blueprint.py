from flask import Blueprint
from app.controllers.restaurant_controller import create_restaurant_address, register_restaurant, restaurant_login, get_all_restaurants_info, get_restaurant_info, update_restaurant_address, update_restaurant_info, delete_restaurant

bp_restaurant = Blueprint('restaurant', __name__, url_prefix='/restaurant')

bp_restaurant.post('/signup')(register_restaurant)
bp_restaurant.post('/signin')(restaurant_login)
bp_restaurant.get('')(get_all_restaurants_info)
bp_restaurant.get('/<int:id>')(get_restaurant_info)
bp_restaurant.patch('')(update_restaurant_info)
bp_restaurant.delete('')(delete_restaurant)
bp_restaurant.patch('/address')(update_restaurant_address)
bp_restaurant.post('/address')(create_restaurant_address)