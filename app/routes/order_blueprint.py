from flask import Blueprint
from app.controllers.order_controllers import delete_dishe_order, get_all_orders, create_new_order, update_order_pratos, update_order_status, delete_order, delete_dishe_order

bp_order = Blueprint("Order", __name__, url_prefix="/order")

bp_order.post("")(create_new_order)
bp_order.get("")(get_all_orders)
bp_order.patch("/<int:id>/dishes")(update_order_pratos)
bp_order.patch("/<int:id>/status")(update_order_status)
bp_order.delete("/<int:order_id>")(delete_order)
bp_order.delete("/<int:order_id>/dishes")(delete_dishe_order)
