<div align="center">

<h1 align="center">Healthfood</h1>

##

O Healthfood é um projeto desenvolvido por alunos da Kenzie Academy Brasil utilizando python para o backend.
Tem como finalidade **fornecer** alimentos de qualidade com o foco na saude.

<br />

</div>

# ✅ Tabela de Conteúdos

<!--ts-->

- [Tecnologias](#tecnologias)
- [Utilitários](#utilitarios)
- [Como rodar a aplicação](#aplicacao)
- [Contribuição](#contribuicao)
- [Documentação](#documentação)

<!--te-->

<br/>

<h1 id="tecnologias"> ✅ Tecnologias </h1>

#### **Tecnologias**

<!--ts-->

- Python
- Flask
- Modelo ORM de organização do banco de dados


<!--te-->

<br/>

### **EndPoints Utilizados**

     /users
     /restaurant
     /orders
     /dishes


<h1 id="utilitarios"> ✅ Utilitários </h1>

<!--ts-->

- [Editor (Visual Studio Code)](https://code.visualstudio.com/download)
- [Trello](https://trello.com/b/348rSPpu/capstone-q3-healthfood)

<!--te-->

<h1 id="aplicacao"> ✅ Como rodar a aplicação </h1>

## Pré-requisitos

**alembic**==1.7.3 <br/>
**cffi**==1.15.0 <br/>
**click**==8.0.1 <br/>
**cryptography**==3.4.8 <br/>
**environs**==9.3.4 <br/>
**Flask**==2.0.2 <br/>
**Flask-HTTPAuth**==4.4.0 <br/>
**Flask-JWT-Extended**==4.2.1 <br/>
**Flask-Migrate**==3.1.0 <br/>
**Flask-SQLAlchemy**==2.5.1 <br/>
**greenlet**==1.1.2 <br/>
**gunicorn**==20.1.0 <br/>
**itsdangerous**==2.0.1 <br/>
**Jinja2**==3.0.2 <br/>
**Mako**==1.1.5 <br/>
**MarkupSafe**==2.0.1 <br/>
**marshmallow**==3.13.0 <br/>
**psycopg2-binary**==2.9.1 <br/>
**pycparser**==2.20 <br/>
**PyJWT**==2.2.0 <br/>
**python-dotenv**==0.19.0 <br/>
**SQLAlchemy**==1.4.25 <br/>
**Werkzeug**==2.0.2 


<br />

## Clonando o repositório do Gitlab

    $ git clone https://gitlab.com/leomarromanzini/healthfood

## Acesse o projeto no terminal/cmd

    $ cd healthfood

## Abra no Visual Studio Code

    $ code .

<br />

<br />

<h1 id="contribuicao">  ✅ Contribuição </h1>

<div align="center">
 <table style="width:100%">
 <tr>
    <th><strong>Itallo Dornelas</strong></th>
    <th><strong>Thiago Henrique Dias Silva</strong></th>
    <th><strong>Leandro Freitas</strong></th>
    <th><strong>Leomar Romanzini</strong></th>
  </tr>
  <tr align="center">
    <td>
      <a href="https://www.linkedin.com/in/itallo-dornelas/" 
         target="_blank"
         rel="noreferrer">
         </a>
    </td>
    <td>
      <a href="https://www.linkedin.com/in/thiagohdsilva/"
         target="_blank"
         rel="noreferrer">
        </a>
    </td>
    <td>
      <a  href="https://www.linkedin.com/in/leandro-sim%C3%B5es/" target="_blank"
        rel="noreferrer">
      </a>
     </td>
      <td>
      <a  href="https://www.linkedin.com/in/leomarromanzini/"target="_blank"
        rel="noreferrer">
      </a>
     </td>
   </tr>
 </table>
</div>

<div>

<h1 id="documentação"> ✅ Documentação </h1>
#### **Documentação**

<h2> ROTA USUÁRIO endpoint='/users'</h2>
- **POST**: Rota para o usuário criar uma conta. Enviar informação do endereço para a model de endereco. <br/>
- **GET**: Rota para o usuário visualizar a informação da conta.<br/>
- **POST**: Rota para o usuário fazer login. <br/>
- **PATCH**: Rota para o usuário atualizar as informações da conta. (PROTEGIDA) <br/>
- **DELETE**: Rota para o usuário deletar a conta. (PROTEGIDA) <br/>
<br/>
  **BODY**{
    "nome" : "Fulano", <br/>
    "telefone" : "0099999-9999", <br/>
    "email" : "fulano@mail.com",<br/>
    "senha" : "1234"
  }

<h2> ROTA RESTAURANT endpoint='/restaurant'</h2>
- **GET**: Rota que permite visualizar todos os restaurantes cadastrados. <br/>
- **POST**: Rota para o restaurante criar uma conta. Enviar informação do endereço para a model de endereco. <br/>
- **POST**: Rota para o restaurante fazer login. <br/>
- **GET**: Rota para o restaurante visualizar informação da própria conta. (PROTEGIDA) <br/>
- **PATCH**: Rota para o restaurante atualizar as informações da conta. (PROTEGIDA) <br/>
- **DELETE**: Rota para o restaurante deletar a conta. (PROTEGIDA) <br/>
<br/>
  **BODY**{
    "nome" : "Dom restaurante", <br/>
    "Estilo Culinário" : "Vegano", <br/>
    "Descrição" : "Um restaurante com comidas que ajudam na melhoria da sua saude", <br/>
    "senha" : "1234"
  }

<h2> ROTA ORDENS endpoint='/orders'</h2>
- **POST**: Criar uma nova ordem. (PROTEGIDA, usuário) <br/>
- **PATCH**: Alterar o status da ordem para cancelada. (PROTEGIDA, usuário) <br/>
- **PATCH**: Alterar o status da ordem. (PROTEGIDA, restaurante) <br/>
- **GET**: Visualizar o status da ordem. (PROTEGIDA, usuário, restaurante) <br/>
<br/>
  **BODY**{
    "status" : "2",
  }

<h2> ROTA PRATOS endpoint='/dishes'</h2>
- **GET**: Visualizar os pratos de um restaurante. <br/>
- **POST**: Criar um novo prato. (PROTEGIDA, restaurante) <br/>
- **PATCH**: Atualizar um prato. (PROTEGIDA, restaurante) <br/>
- **DELETE**: Deletar um prato. (PROTEGIDA, restaurante) <br/>
<br/>
  **BODY**{
    "Nome" : "Feijoada", <br/>
    "Ingredientes" : "feijao e arroz", <br/>
    "Preço" : "30.5", <br/>
    "Calorias" : "30", <br/>
    "Descrição: "Comida tipica com muito sabor"
  }
</div>

